//
//  FDWebInterface.m
//
//  Created by Hyunjae on 24/03/15.
//

#import "FDWebInterface.h"

static NSString * const kFDBaseURLString = @"http://findo-test.herokuapp.com/api/1.0/";

@implementation FDWebInterface

+(instancetype)sharedClient
{
    static FDWebInterface *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[FDWebInterface alloc] initWithBaseURL:[NSURL URLWithString:kFDBaseURLString]];
        _sharedClient.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    });
    
    if( [self isReachable] )
        return _sharedClient;
    else{
        UIAlertView *noConnectionAlert = [[UIAlertView alloc] initWithTitle:@"No Connection" message:@"Connection seems to be lost." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [noConnectionAlert show];
        return nil;
    }
}

+(BOOL)isReachable
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    return networkStatus == NotReachable ? FALSE : TRUE;
}

@end
