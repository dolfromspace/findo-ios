//
//  OpeningHours.m
//  Findo
//
//  Created by Hyunjae on 27/03/15.
//  Copyright (c) 2015 Chromic. All rights reserved.
//

#import "OpeningHours.h"
#import "StoreInfo.h"


@implementation OpeningHours

@dynamic fri_close;
@dynamic fri_open;
@dynamic mon_close;
@dynamic mon_open;
@dynamic sat_close;
@dynamic sat_open;
@dynamic sun_close;
@dynamic sun_open;
@dynamic thur_close;
@dynamic thur_open;
@dynamic tue_close;
@dynamic tue_open;
@dynamic wed_close;
@dynamic wed_open;
@dynamic store;

-(BOOL)isOpenToday
{
    BOOL result = FALSE;
    NSDate *currenDate = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"c"];
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber *currentDay = [f numberFromString:[dateFormatter stringFromDate:currenDate]];
    
    [dateFormatter setDateFormat:@"kk"];
    NSNumber *currentHour = [f numberFromString:[dateFormatter stringFromDate:currenDate]];
    
    [dateFormatter setDateFormat:@"mm"];
    NSNumber *currentMinute = [f numberFromString:[dateFormatter stringFromDate:currenDate]];

    NSString* openHour, *closeHour;
    
    switch( currentDay.intValue )
    {
        case 1:
            openHour = self.sun_open;
            closeHour = self.sun_close;
            break;
        case 2:
            openHour = self.mon_open;
            closeHour = self.mon_close;
            break;
        case 3:
            openHour = self.tue_open;
            closeHour = self.tue_close;
            break;
        case 4:
            openHour = self.wed_open;
            closeHour = self.wed_close;
            break;
        case 5:
            openHour = self.thur_open;
            closeHour = self.thur_close;
            break;
        case 6:
            openHour = self.fri_open;
            closeHour = self.fri_close;
            break;
        case 7:
            openHour = self.sat_open;
            closeHour = self.sat_close;
            break;
        default:
            break;
    }
    
    if( [openHour isEqualToString:@"close"] && [openHour isEqualToString:@"open"] )
        return false;
    
    [dateFormatter setDateFormat:@"HH:mmZZZ"];
    NSDate *rawOpenDate = [dateFormatter dateFromString:openHour];
    NSDate *rawCloseDate = [dateFormatter dateFromString:closeHour];
    [dateFormatter setDateFormat:@"HH"];
    NSNumber *openHourNumber = [f numberFromString:[dateFormatter stringFromDate:rawOpenDate]];
    NSNumber *closeHourNumber = [f numberFromString:[dateFormatter stringFromDate:rawCloseDate]];
    [dateFormatter setDateFormat:@"mm"];
    NSNumber *openMinute = [f numberFromString:[dateFormatter stringFromDate:rawOpenDate]];
    NSNumber *closeMinute = [f numberFromString:[dateFormatter stringFromDate:rawCloseDate]];
    
    NSNumber *currentTimeInMinutes = [NSNumber numberWithInt:currentHour.intValue * 60 + currentMinute.intValue];
    NSNumber *openTimeInMinutes = [NSNumber numberWithInt:openHourNumber.intValue * 60 + openMinute.intValue];
    NSNumber *closeTimeInMinutes = [NSNumber numberWithInt:closeHourNumber.intValue * 60 + closeMinute.intValue];
    
    result = (currentTimeInMinutes > openTimeInMinutes && currentTimeInMinutes < closeTimeInMinutes);
    
    return result;
}
@end
