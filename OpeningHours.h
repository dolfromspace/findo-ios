//
//  OpeningHours.h
//  Findo
//
//  Created by Hyunjae on 27/03/15.
//  Copyright (c) 2015 Chromic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "StoreInfo.h"

@class StoreInfo;

@interface OpeningHours : StoreInfo

@property (nonatomic, retain) NSString * fri_close;
@property (nonatomic, retain) NSString * fri_open;
@property (nonatomic, retain) NSString * mon_close;
@property (nonatomic, retain) NSString * mon_open;
@property (nonatomic, retain) NSString * sat_close;
@property (nonatomic, retain) NSString * sat_open;
@property (nonatomic, retain) NSString * sun_close;
@property (nonatomic, retain) NSString * sun_open;
@property (nonatomic, retain) NSString * thur_close;
@property (nonatomic, retain) NSString * thur_open;
@property (nonatomic, retain) NSString * tue_close;
@property (nonatomic, retain) NSString * tue_open;
@property (nonatomic, retain) NSString * wed_close;
@property (nonatomic, retain) NSString * wed_open;
@property (nonatomic, retain) StoreInfo *store;

-(BOOL)isOpenToday;

@end
