//
//  main.m
//  Findo
//
//  Created by Hyunjae on 24/03/15.
//  Copyright (c) 2015 Chromic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FDAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([FDAppDelegate class]));
    }
}
