//
//  StoreInfo.h
//  
//
//  Created by Hyunjae on 28/04/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "MasterDB.h"

@class Address, MasterDB, Menu, OpeningHours;

@interface StoreInfo : MasterDB

@property (nonatomic, retain) NSString * car_park;
@property (nonatomic, retain) NSString * category;
@property (nonatomic, retain) NSString * credit_card;
@property (nonatomic, retain) NSString * highlight;
@property (nonatomic, retain) NSString * identity;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSString * primary_name;
@property (nonatomic, retain) NSString * secondary_name;
@property (nonatomic, retain) NSString * tags;
@property (nonatomic, retain) NSString * thumbnail;
@property (nonatomic, retain) NSNumber * active;
@property (nonatomic, retain) Address *address;
@property (nonatomic, retain) MasterDB *master;
@property (nonatomic, retain) NSOrderedSet *menu;
@property (nonatomic, retain) OpeningHours *opening_hours;
-(BOOL)SearchForText:(NSString*) searchText;
@end

@interface StoreInfo (CoreDataGeneratedAccessors)

- (void)insertObject:(Menu *)value inMenuAtIndex:(NSUInteger)idx;
- (void)removeObjectFromMenuAtIndex:(NSUInteger)idx;
- (void)insertMenu:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeMenuAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInMenuAtIndex:(NSUInteger)idx withObject:(Menu *)value;
- (void)replaceMenuAtIndexes:(NSIndexSet *)indexes withMenu:(NSArray *)values;
- (void)addMenuObject:(Menu *)value;
- (void)removeMenuObject:(Menu *)value;
- (void)addMenu:(NSOrderedSet *)values;
- (void)removeMenu:(NSOrderedSet *)values;
@end
