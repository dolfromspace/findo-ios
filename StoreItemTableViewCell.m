//
//  StoreItemTableViewCell.m
//  Findo
//
//  Created by Hyunjae on 26/04/15.
//  Copyright (c) 2015 Chromic. All rights reserved.
//

#import "StoreItemTableViewCell.h"

@implementation StoreItemTableViewCell
@synthesize storeImage, storeLoc, storeName, storePhone, storeSecName, storeStatus, separator, uivHeaderIndicator, uivNoImage;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setCurrentStateIfOpen:(BOOL)isOpen
{
    [uivHeaderIndicator setBackgroundColor:isOpen ? [UIColor colorWithRed:41.0/255.0 green:211.0/255.0 blue:146.0/255.0 alpha:1] : [UIColor colorWithRed:234.0/255.0 green:57.0/255.0 blue:57/255.0 alpha:1]];
    [storeStatus setText:isOpen ? @"OPEN" : @"CLOSED"];
    [storeStatus setTextColor:isOpen ? [UIColor colorWithRed:41.0/255.0 green:211.0/255.0 blue:146.0/255.0 alpha:1] : [UIColor colorWithRed:234.0/255.0 green:57.0/255.0 blue:57/255.0 alpha:1]];
    [storeStatus setFont:[UIFont boldSystemFontOfSize:11]];
}


@end
