//
//  Menu.m
//  Findo
//
//  Created by Hyunjae on 27/03/15.
//  Copyright (c) 2015 Chromic. All rights reserved.
//

#import "Menu.h"
#import "StoreInfo.h"


@implementation Menu

@dynamic menu_name;
@dynamic menu_price;
@dynamic store;

@end
