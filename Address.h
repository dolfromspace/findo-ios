//
//  Address.h
//  
//
//  Created by Hyunjae on 28/04/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "StoreInfo.h"

@class StoreInfo;

@interface Address : StoreInfo

@property (nonatomic, retain) NSString * city;
@property (nonatomic, retain) NSString * line1;
@property (nonatomic, retain) NSString * line2;
@property (nonatomic, retain) NSString * postcode;
@property (nonatomic, retain) NSString * suburb;
@property (nonatomic, retain) StoreInfo *store;

@end
