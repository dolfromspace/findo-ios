//
//  StoreInfo.m
//  
//
//  Created by Hyunjae on 28/04/15.
//
//

#import "StoreInfo.h"
#import "Address.h"
#import "MasterDB.h"
#import "Menu.h"
#import "OpeningHours.h"


@implementation StoreInfo

@dynamic car_park;
@dynamic category;
@dynamic credit_card;
@dynamic highlight;
@dynamic identity;
@dynamic phone;
@dynamic primary_name;
@dynamic secondary_name;
@dynamic tags;
@dynamic thumbnail;
@dynamic active;
@dynamic address;
@dynamic master;
@dynamic menu;
@dynamic opening_hours;

-(BOOL)SearchForText:(NSString*) searchText
{
    BOOL result = FALSE;
    NSString * prName = self.primary_name;
    NSString * seName = self.secondary_name;
    
    if( [prName containsString:searchText] || [seName containsString:searchText] )
        result = TRUE;
    
    return result;
}
@end
