//
//  Address.m
//  
//
//  Created by Hyunjae on 28/04/15.
//
//

#import "Address.h"
#import "StoreInfo.h"


@implementation Address

@dynamic city;
@dynamic line1;
@dynamic line2;
@dynamic postcode;
@dynamic suburb;
@dynamic store;

@end
