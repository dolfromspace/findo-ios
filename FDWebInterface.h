//
//  FDWebInterface.h
//
//  Created by Hyunjae on 24/03/15.
//

#import <Foundation/Foundation.h>
#import "AFNetworking/AFHTTPSessionManager.h"
#import "AFNetworkReachabilityManager.h"
#import "Reachability.h"

@interface FDWebInterface : AFHTTPSessionManager

+ (instancetype)sharedClient;
+ (BOOL) isReachable;

@end
