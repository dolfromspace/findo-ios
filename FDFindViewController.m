//
//  FDFindViewController.m
//  Findo
//
//  Created by Hyunjae on 25/04/15.
//  Copyright (c) 2015 Chromic. All rights reserved.
//

#import "FDFindViewController.h"
#import "FDMasterViewController.h"
#import "UIImage+UIImage_ColorImage.h"
#import "StoreItemTableViewCell.h"
#import "Address.h"
#import "StoreInfo.h"
#import "MenuView.h"
#import "OpeningHours.h"

#define storeArray (((FDMasterViewController*)self.parentViewController).maStoreInfo)

@interface FDFindViewController ()
{
    BOOL bSearching;
}
@end

@implementation FDFindViewController
@synthesize uisbSearchBar, uitvItemList, filteredItemList;



- (void)viewDidLoad {
    bSearching = FALSE;
    [super viewDidLoad];
    CGRect searchBarRect = CGRectMake(0, 20, [[UIScreen mainScreen] bounds].size.width, uisbSearchBar.frame.size.height);
    CGRect itemListRect = CGRectMake(4, uisbSearchBar.frame.size.height + 20, [[UIScreen mainScreen] bounds].size.width - 8, ([[UIScreen mainScreen] bounds].size.height - (uisbSearchBar.frame.size.height + 60 + 20)));
    if( uisbSearchBar == nil )
        uisbSearchBar = [[UISearchBar alloc] initWithFrame:searchBarRect];
    
    if( uitvItemList == nil )
        uitvItemList = [[UITableView alloc] initWithFrame:itemListRect];
    
    [uisbSearchBar setFrame:searchBarRect];
    [uisbSearchBar setPlaceholder:@"Find the place"];
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setBackgroundColor:[UIColor colorWithRed:43.0/255.0 green:43.0/255.0 blue:43.0/255.0 alpha:1]];
   /* UIImage *backimage = [UIImage imageWithColor:[UIColor colorWithRed:43.0/255.0 green:43.0/255.0 blue:43.0/255.0 alpha:1]];
    backimage = [UIImage imageWithImage:backimage scaledToSize:CGSizeMake([[UIScreen mainScreen] bounds].size.width, 20)];
    [uisbSearchBar setSearchFieldBackgroundImage:backimage forState:UIControlStateNormal];*/
    [uitvItemList setFrame:itemListRect ];
    uitvItemList.backgroundColor = [UIColor colorWithRed:66.0/255.0 green:66.0/255.0 blue:66.0/255.0 alpha:1];
    NSLog(@"%f %f %f %f", [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height, self.view.frame.size.height, self.view.frame.size.width);
    // Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor colorWithRed:66.0/255.0 green:66.0/255.0 blue:66.0/255.0 alpha:1]];
    [uisbSearchBar setDelegate:self];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)updateData
{
    self.filteredItemList = [NSMutableArray arrayWithCapacity:[storeArray count]];
    [uitvItemList reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int count = 0;
    NSMutableArray * arrayToUse;
    
    if( bSearching )
        arrayToUse = filteredItemList;
    else
        arrayToUse = storeArray;
    
    for( StoreInfo * store in arrayToUse )
        if( store.active.boolValue )
            count++;
    
    return count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    StoreInfo *infoToDisplay;
    if( bSearching )
        infoToDisplay = [filteredItemList objectAtIndex:indexPath.item];
    else
        infoToDisplay = [storeArray objectAtIndex:indexPath.item];
    
    StoreItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"StoreItemTableViewCell"];
    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"StoreItemTableViewCell" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
        cell.backgroundColor = [UIColor colorWithRed:31.0/255.0 green:31.0/255.0 blue:31.0/255.0 alpha:1];
        cell.separator.backgroundColor = [UIColor colorWithRed:66.0/255.0 green:66.0/255.0 blue:66.0/255.0 alpha:1];
        cell.storeImage.layer.cornerRadius = 33;
        cell.storeImage.layer.masksToBounds = YES;
        cell.uivNoImage.layer.cornerRadius = 33;
        cell.uivNoImage.layer.masksToBounds = YES;
        [cell.uivNoImage setBackgroundColor:[UIColor colorWithRed:66.0/255.0 green:66.0/255.0 blue:66.0/255.0 alpha:1]];
        [cell.storeName setText:infoToDisplay.primary_name];
        [cell.storeSecName setText:infoToDisplay.secondary_name];
        [cell.storePhone setText:infoToDisplay.phone];
        [cell.storeLoc setText:infoToDisplay.address.city];
        [cell setCurrentStateIfOpen:infoToDisplay.opening_hours.isOpenToday];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return cell;
}



- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    // You'll probably want to do this on another thread
    // SomeService is just a dummy class representing some
    // api that you are using to do the search
    for( StoreInfo * storeToSearch in storeArray )
    {
        if( [storeToSearch SearchForText:searchBar.text] )
           [filteredItemList addObject:storeToSearch];
    }
    
    [searchBar setShowsCancelButton:NO animated:YES];
    [searchBar resignFirstResponder];
    uitvItemList.allowsSelection = YES;
    uitvItemList.scrollEnabled = YES;
    bSearching = TRUE;
    
    [uitvItemList reloadData];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    searchBar.text=@"";
    
    [searchBar setShowsCancelButton:NO animated:YES];
    [searchBar resignFirstResponder];
    bSearching = FALSE;
    [uitvItemList reloadData];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:YES animated:YES];
}

#pragma mark Content Filtering
-(void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope {
    // Update the filtered array based on the search text and scope.
    // Remove all objects from the filtered search array
    [self.filteredItemList removeAllObjects];
    // Filter the array using NSPredicate
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.name contains[c] %@",searchText];
    filteredItemList = [NSMutableArray arrayWithArray:[storeArray filteredArrayUsingPredicate:predicate]];
}

#pragma mark - UISearchDisplayController Delegate Methods
-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
    // Tells the table data source to reload when text changes
    [self filterContentForSearchText:searchString scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption {
    // Tells the table data source to reload when scope bar selection changes
    [self filterContentForSearchText:self.searchDisplayController.searchBar.text scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:searchOption]];
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
