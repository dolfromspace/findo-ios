//
//  FDSyncEngine.m
//  Findo
//
//  Created by Hyunjae on 24/03/15.
//  Copyright (c) 2015 Chromic. All rights reserved.
//

#import "FDSyncEngine.h"
#import "FDAppDelegate.h"
#import "MasterDB.h"
#import "Address.h"
#import "StoreInfo.h"
#import "OpeningHours.h"
#import "Menu.h"

@implementation FDSyncEngine
@synthesize managedObjectContext = __managedObjectContext;
@synthesize managedObjectModel = __managedObjectModel;
@synthesize persistentStoreCoordinator = __persistentStoreCoordinator;

+ (FDSyncEngine *)sharedEngine {
    static FDSyncEngine *sharedEngine = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedEngine = [[FDSyncEngine alloc] init];
    });
    
    return sharedEngine;
}

-(MasterDB*)FetchMasterDatabase:(NSManagedObjectContext*)context
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"MasterDB"
                                              inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:nil];
    MasterDB* oneAndOnlyMDB;
    for (MasterDB *info in fetchedObjects) {
        oneAndOnlyMDB = info;
        break;
    }
    
    return oneAndOnlyMDB;
}

-(NSManagedObjectContext*)offlineSync
{
    NSManagedObjectContext *context = [self managedObjectContext];
    MasterDB* oneAndOnlyMDB = [self FetchMasterDatabase:context];
    @try {
        if( [oneAndOnlyMDB.stores count] > 0 )
        {
            NSLog(@"Showing all store infos");
            for( StoreInfo* storeinfo in oneAndOnlyMDB.stores )
            {
                NSLog(@"Store Identity: %@", storeinfo.identity);
                NSLog(@"Store highlight: %@", storeinfo.highlight);
                NSLog(@"Store primary_name: %@", storeinfo.primary_name);
                NSLog(@"Store secondary_name: %@", storeinfo.secondary_name);
                NSLog(@"Store category: %@", storeinfo.category);
                NSLog(@"Store phone: %@", storeinfo.phone);
                NSLog(@"Store address: %@ %@ %@ %@", storeinfo.address.line1, storeinfo.address.line2, storeinfo.address.city, storeinfo.address.postcode);
                NSLog(@"Store opening_hours: %@", storeinfo.opening_hours.mon_open);
                NSLog(@"Store credit_card: %@", storeinfo.credit_card);
                NSLog(@"Store car_park: %@", storeinfo.car_park);
                NSLog(@"Store tags: %@", storeinfo.tags);
                
            }
        }
        else
        {
            UIAlertView *noDataOffline = [[UIAlertView alloc] initWithTitle:@"Error" message:@"No Data Available for offline view" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [noDataOffline show];
        }
        return context;
    }
    @catch (NSException *exception)
    {
        return nil;
    }
}

-(NSManagedObjectContext*)onlineSync:(id)jsonObject
{
    NSError* localError = nil;
    NSMutableArray *jsonArray = [NSMutableArray arrayWithArray:jsonObject];
    NSError *errors;
    NSManagedObjectContext *context = [self managedObjectContext];
    MasterDB* oneAndOnlyMDB = [self FetchMasterDatabase:context];
    
    
    @try {
        if( [oneAndOnlyMDB.stores count] > 0 )
        {
            NSLog(@"Showing all store infos");
            for( StoreInfo* storeinfo in oneAndOnlyMDB.stores )
            {
                NSLog(@"Store Identity: %@", storeinfo.identity);
                NSLog(@"Store highlight: %@", storeinfo.highlight);
                NSLog(@"Store primary_name: %@", storeinfo.primary_name);
                NSLog(@"Store secondary_name: %@", storeinfo.secondary_name);
                NSLog(@"Store category: %@", storeinfo.category);
                NSLog(@"Store phone: %@", storeinfo.phone);
                NSLog(@"Store address: %@ %@ %@ %@", storeinfo.address.line1, storeinfo.address.line2, storeinfo.address.city, storeinfo.address.postcode);
                NSLog(@"Store opening_hours: %@", storeinfo.opening_hours.mon_open);
                NSLog(@"Store opening_hours: %@", storeinfo.opening_hours.mon_close);
                NSLog(@"Store opening_hours: %@", storeinfo.opening_hours.tue_open);
                NSLog(@"Store opening_hours: %@", storeinfo.opening_hours.tue_close);
                NSLog(@"Store opening_hours: %@", storeinfo.opening_hours.wed_open);
                NSLog(@"Store opening_hours: %@", storeinfo.opening_hours.wed_close);
                NSLog(@"Store opening_hours: %@", storeinfo.opening_hours.thur_open);
                NSLog(@"Store opening_hours: %@", storeinfo.opening_hours.thur_close);
                NSLog(@"Store opening_hours: %@", storeinfo.opening_hours.fri_open);
                NSLog(@"Store opening_hours: %@", storeinfo.opening_hours.fri_close);
                NSLog(@"Store opening_hours: %@", storeinfo.opening_hours.sat_open);
                NSLog(@"Store opening_hours: %@", storeinfo.opening_hours.sat_close);
                NSLog(@"Store opening_hours: %@", storeinfo.opening_hours.sun_open);
                NSLog(@"Store opening_hours: %@", storeinfo.opening_hours.sun_close);
                NSLog(@"Store credit_card: %@", storeinfo.credit_card);
                NSLog(@"Store car_park: %@", storeinfo.car_park);
                NSLog(@"Store tags: %@", storeinfo.tags);
                
            }
        }
        else
        {
            MasterDB *masterDB = [NSEntityDescription
                                  insertNewObjectForEntityForName:@"MasterDB"
                                  inManagedObjectContext:context];
            if (localError != nil) {
                return nil;
            }
            
            NSMutableOrderedSet *mutableItems = [[NSMutableOrderedSet alloc] init];
            for( NSDictionary* storeinfo in jsonArray)
            {
                StoreInfo *storeToAdd = [NSEntityDescription insertNewObjectForEntityForName:@"StoreInfo" inManagedObjectContext:context];
                storeToAdd.identity = [storeinfo objectForKey:@"_id"];
                storeToAdd.highlight = [storeinfo objectForKey:@"highlight"];
                storeToAdd.primary_name = [storeinfo objectForKey:@"primary_name"];
                storeToAdd.secondary_name = [storeinfo objectForKey:@"secondary_name"];
                storeToAdd.category = [storeinfo objectForKey:@"category"];
                storeToAdd.phone = [storeinfo objectForKey:@"phone"];
                Address *addressToAdd = [NSEntityDescription insertNewObjectForEntityForName:@"Address" inManagedObjectContext:context];
                addressToAdd.line1 = [[storeinfo objectForKey:@"address"] objectForKey:@"line1"];
                addressToAdd.line2 = [[storeinfo objectForKey:@"address"] objectForKey:@"line2"];
                addressToAdd.city = [[storeinfo objectForKey:@"address"] objectForKey:@"city"];
                addressToAdd.postcode = [[storeinfo objectForKey:@"address"] objectForKey:@"postcode"];
                addressToAdd.suburb = [[storeinfo objectForKey:@"address"] objectForKey:@"address"];
                storeToAdd.address = addressToAdd;
                OpeningHours *hoursToAdd = [NSEntityDescription insertNewObjectForEntityForName:@"OpeningHours" inManagedObjectContext:context];
                hoursToAdd.mon_open = [[storeinfo objectForKey:@"opening_hours"] objectForKey:@"mon_open"];
                hoursToAdd.mon_close = [[storeinfo objectForKey:@"opening_hours"] objectForKey:@"mon_close"];
                hoursToAdd.tue_open = [[storeinfo objectForKey:@"opening_hours"] objectForKey:@"tue_open"];
                hoursToAdd.tue_close = [[storeinfo objectForKey:@"opening_hours"] objectForKey:@"tue_close"];
                hoursToAdd.wed_open = [[storeinfo objectForKey:@"opening_hours"] objectForKey:@"wed_open"];
                hoursToAdd.wed_close = [[storeinfo objectForKey:@"opening_hours"] objectForKey:@"wed_close"];
                hoursToAdd.thur_open = [[storeinfo objectForKey:@"opening_hours"] objectForKey:@"thur_open"];
                hoursToAdd.thur_close = [[storeinfo objectForKey:@"opening_hours"] objectForKey:@"thur_close"];
                hoursToAdd.fri_open = [[storeinfo objectForKey:@"opening_hours"] objectForKey:@"fri_open"];
                hoursToAdd.fri_close = [[storeinfo objectForKey:@"opening_hours"] objectForKey:@"fri_close"];
                hoursToAdd.sat_open = [[storeinfo objectForKey:@"opening_hours"] objectForKey:@"sat_open"];
                hoursToAdd.sat_close = [[storeinfo objectForKey:@"opening_hours"] objectForKey:@"sat_close"];
                hoursToAdd.sun_open = [[storeinfo objectForKey:@"opening_hours"] objectForKey:@"sun_open"];
                hoursToAdd.sun_close = [[storeinfo objectForKey:@"opening_hours"] objectForKey:@"sun_close"];
                storeToAdd.opening_hours = hoursToAdd;
                storeToAdd.credit_card = [storeinfo objectForKey:@"credit_card"];
                storeToAdd.car_park = [storeinfo objectForKey:@"car_park"];
                storeToAdd.active = [storeinfo objectForKey:@"active"];
                storeToAdd.thumbnail = [storeinfo objectForKey:@"thumbnail"];
                
                storeToAdd.tags = [storeinfo objectForKey:@"tags"];
                
                NSMutableOrderedSet *mutableMenus = [[NSMutableOrderedSet alloc] init];
                NSDictionary *menuDic = [storeinfo objectForKey:@"menu"];
                for( NSString *key in menuDic )
                {
                    NSString * price = [menuDic objectForKey:key];
                    Menu* menu = [NSEntityDescription insertNewObjectForEntityForName:@"Menu" inManagedObjectContext:context];
                    menu.menu_name = key;
                    menu.menu_price = price;
                    [mutableMenus addObject:menu];
                }
                storeToAdd.menu = mutableMenus;
                
                [mutableItems addObject:storeToAdd];
                masterDB.stores = (NSOrderedSet*)mutableItems.copy;
                
            }
        }
        
        NSError *error;
        if (![context save:&error]) {
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }
    }
    @catch (NSException *exception) {
        UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Error syncing server data." delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
        [alertView show];
        return nil;
    }
    
    return context;
}

-(NSManagedObjectContext*)syncDatabaseWith:(id)jsonObject error:(NSError**)error
{
    if( jsonObject == nil )
        return [self offlineSync];
    else
        return [self onlineSync:jsonObject];
    
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (__managedObjectContext != nil) {
        return __managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        __managedObjectContext = [[NSManagedObjectContext alloc] init];
        [__managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return __managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (__managedObjectModel != nil) {
        return __managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"FindoDatabase" withExtension:@"momd"];
    __managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return __managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (__persistentStoreCoordinator != nil) {
        return __persistentStoreCoordinator;
    }
    NSURL* appDelegateURI = [(FDAppDelegate*)[[UIApplication sharedApplication] delegate] applicationDocumentsDirectory];
    NSURL *storeURL = [appDelegateURI URLByAppendingPathComponent:@"FindoDatabase.sqlite"];
    
    NSError *error = nil;
    __persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![__persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption, [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return __persistentStoreCoordinator;
}

@end
