//
//  StoreItemTableViewCell.h
//  Findo
//
//  Created by Hyunjae on 26/04/15.
//  Copyright (c) 2015 Chromic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StoreItemTableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UIImageView *storeImage;
@property (nonatomic, weak) IBOutlet UILabel *storeName;
@property (nonatomic, weak) IBOutlet UILabel *storeSecName;
@property (nonatomic, weak) IBOutlet UILabel *storePhone;
@property (nonatomic, weak) IBOutlet UILabel *storeStatus;
@property (nonatomic, weak) IBOutlet UILabel *storeLoc;
@property (nonatomic, weak) IBOutlet UIView *separator;
@property (nonatomic, weak) IBOutlet UIView *uivHeaderIndicator;
@property (nonatomic, weak) IBOutlet UIView *uivNoImage;
- (void)setCurrentStateIfOpen:(BOOL)isOpen;
@end
