//
//  FBCDAppDelegate.h
//

#import <UIKit/UIKit.h>

@interface FDAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

- (NSURL *)applicationDocumentsDirectory;

@end
