//
//  MenuView.h
//  Findo
//
//  Created by Hyunjae on 20/04/15.
//  Copyright (c) 2015 Chromic. All rights reserved.
//

#import <UIKit/UIKit.h>
@class FDMasterViewController;

@interface MenuView : UIView
{
    @public FDMasterViewController* parent;
}
- (id) initWithCustomFrame;

@property (nonatomic, weak) IBOutlet UIButton *uiButtonHome;
@property (nonatomic, retain) IBOutlet UIButton *uiButtonCoupon;
@property (nonatomic, weak) IBOutlet UIButton *uiButtonFind;
@property (nonatomic, weak) IBOutlet UIButton *uiButtonMore;
-(IBAction) ibActionHome:(id) sender;
-(IBAction) ibActionCoupon:(id) sender;
-(IBAction) ibActionFind:(id) sender;
-(IBAction) ibActionMore:(id) sender;
-(void) highLightThisMenu:(NSInteger)index;
@end
