//
//  FDMasterViewController.h
//
//

#import <UIKit/UIKit.h>

@class MenuView;
@interface FDMasterViewController : UINavigationController

@property (nonatomic, strong) IBOutlet MenuView* menuview;
@property (nonatomic,strong) NSManagedObjectContext* managedObjectContext;
@property (nonatomic,strong) NSMutableArray* maStoreInfo;
-(void)pushSecondController;
-(void)updateData;
-(void)initBaseData;
@end
