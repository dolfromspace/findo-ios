//
//  FDSyncEngine.h
//  Findo
//
//  Created by Hyunjae on 24/03/15.
//  Copyright (c) 2015 Chromic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface FDSyncEngine : NSObject
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

+ (FDSyncEngine *)sharedEngine;
- (void)saveContext;
- (NSManagedObjectContext*)syncDatabaseWith:(id) jsonObject error:(NSError**)error;
- (NSManagedObjectContext*)onlineSync:(id)jsonObject;
- (NSManagedObjectContext*)offlineSync;
@end
