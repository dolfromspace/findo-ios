//
//  Menu.h
//  Findo
//
//  Created by Hyunjae on 27/03/15.
//  Copyright (c) 2015 Chromic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "StoreInfo.h"

@class StoreInfo;

@interface Menu : StoreInfo

@property (nonatomic, retain) NSString * menu_name;
@property (nonatomic, retain) NSString * menu_price;
@property (nonatomic, retain) StoreInfo *store;

@end
