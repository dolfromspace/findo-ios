//
//  FDFindViewController.h
//  Findo
//
//  Created by Hyunjae on 25/04/15.
//  Copyright (c) 2015 Chromic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FDFindViewController : UIViewController <UISearchBarDelegate, UISearchDisplayDelegate>
@property (nonatomic, strong) IBOutlet UISearchBar *uisbSearchBar;
@property (nonatomic, strong) IBOutlet UITableView *uitvItemList;
@property (strong,nonatomic) NSMutableArray *filteredItemList;
-(void)updateData;
@end
