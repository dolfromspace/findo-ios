//
//  UIImage+UIImage_ColorImage.h
//  Findo
//
//  Created by Hyunjae on 26/04/15.
//  Copyright (c) 2015 Chromic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (UIImage_ColorImage)
+ (UIImage *)imageWithColor:(UIColor *)color;
+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;
@end
