//
//  FBCDAppDelegate.m
//
//

#import "FDAppDelegate.h"
#import "FDMasterViewController.h"
#import "FDWebInterface.h"
#import "FDSyncEngine.h"
#import "Address.h"
#import "StoreInfo.h"
#import "OpeningHours.h"

@implementation FDAppDelegate

@synthesize window = _window;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSURLCache *URLCache = [[NSURLCache alloc] initWithMemoryCapacity:4 * 1024 * 1024 diskCapacity:20 * 1024 * 1024 diskPath:nil];
    [NSURLCache setSharedURLCache:URLCache];
    
    FDMasterViewController *navigationController = (FDMasterViewController *)self.window.rootViewController;
    [navigationController initBaseData];
    
    FDWebInterface * FDWI = [FDWebInterface sharedClient];
    if( FDWI != nil )
    {
        [FDWI GET:@"store/list" parameters:nil success:^(NSURLSessionDataTask * __unused task, NSData* JSON) {
            navigationController.managedObjectContext = [[FDSyncEngine sharedEngine] syncDatabaseWith:JSON error:nil];
            [navigationController updateData ];
            
        } failure:^(NSURLSessionDataTask *__unused task, NSError *error) {
            NSString* desc = error.description;
        }];
    }
    else
        [[FDSyncEngine sharedEngine] syncDatabaseWith:nil error:nil];
    // Override point for customization after application launch.
    /*UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;
    FDMasterViewController *controller = (FDMasterViewController *)navigationController.topViewController;*/
    //controller.managedObjectContext = self.managedObjectContext;
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
   // [self saveContext];
}


#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end
