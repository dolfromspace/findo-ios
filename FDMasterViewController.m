//
//  FDMasterViewController.m
//
//
//  Created by Hyunjae Lee.
//

#import "FDMasterViewController.h"
#import "FDFindViewController.h"
#import "Address.h"
#import "StoreInfo.h"
#import "MenuView.h"
#import "OpeningHours.h"

@interface FDMasterViewController ()
{
    
    
}
@end

@implementation FDMasterViewController
@synthesize managedObjectContext;
@synthesize maStoreInfo;
@synthesize menuview;

-(id)init
{
    FDMasterViewController* var = [super init];
    [var.view setFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height)];
    
    return var;
}

-(void)initBaseData
{
    maStoreInfo = [[NSMutableArray alloc] init];
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
   menuview = [[MenuView alloc] initWithCustomFrame];
    menuview->parent = self;
    [self.view setFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height)];
    [self.view addSubview:menuview];
    
    [self.view bringSubviewToFront:menuview];
    
    NSArray *currentControllers = self.viewControllers;
    
    UIView * statusView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 20)];
    [statusView setBackgroundColor:[UIColor colorWithRed:(26.0/255.0) green:(26.0/255.0) blue:(26.0/255.0) alpha:1]];

    [self.view sendSubviewToBack:statusView];
    [self.view addSubview:statusView];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)pushSecondController
{
    NSArray *currentControllers = self.navigationController.viewControllers;
    if( currentControllers != nil )
        NSLog(@"hi");
    UIStoryboard*  sb = [UIStoryboard storyboardWithName:@"Main"
                                                  bundle:nil];
    UIViewController* vc = [sb instantiateViewControllerWithIdentifier:@"FindVIewController"];
    [self pushViewController:vc animated:YES];
}

-(void)updateData
{
    [menuview highLightThisMenu:1];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
    entityForName:@"MasterDB" inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    NSError *error;
    MasterDB * masterdb =(MasterDB*)[[managedObjectContext executeFetchRequest:fetchRequest error:&error] objectAtIndex:0];
    for( StoreInfo *storeToAdd in [masterdb.stores array] )
    {
        if( storeToAdd.active.boolValue )
            [maStoreInfo addObject:storeToAdd];
    }

    id  vc = [self topViewController];
    [vc updateData];
}

#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

@end
