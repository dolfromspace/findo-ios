//
//  MenuView.m
//  Findo
//
//  Created by Hyunjae on 20/04/15.
//  Copyright (c) 2015 Chromic. All rights reserved.
//

#import "MenuView.h"
#import "FDMasterViewController.h"

@implementation MenuView
@synthesize uiButtonCoupon, uiButtonFind, uiButtonHome, uiButtonMore;
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
 */
- (id) initWithCustomFrame
{
    NSArray *array;
    array = [[NSBundle mainBundle]loadNibNamed:@"MenuView" owner:self options:nil];
    
    self = [array objectAtIndex:0];
    [self setFrame:CGRectMake(0, [[UIScreen mainScreen] bounds].size.height - 60, [[UIScreen mainScreen] bounds].size.width, 60)];
    [uiButtonHome setCenter:CGPointMake([[UIScreen mainScreen] bounds].size.width / 8 , self.frame.size.height / 2 )];
    [uiButtonCoupon setCenter:CGPointMake([[UIScreen mainScreen] bounds].size.width * 3 / 8 , self.frame.size.height / 2)];
    [uiButtonFind setCenter:CGPointMake([[UIScreen mainScreen] bounds].size.width * 5 / 8 , self.frame.size.height / 2 )];
    [uiButtonMore setCenter:CGPointMake([[UIScreen mainScreen] bounds].size.width * 7 / 8 , self.frame.size.height / 2 )];
    NSLog(@"%f %f %f %f", [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height, self.frame.size.height, self.frame.size.width);
    return self;
    
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if ((self = [super initWithCoder:aDecoder])) {
    }
    return self;
}

-(IBAction) ibActionHome:(id) sender
{
}
-(IBAction) ibActionCoupon:(id) sender
{
    
}
-(IBAction) ibActionFind:(id) sender
{
    
}
-(IBAction) ibActionMore:(id) sender
{
    
}
-(void) highLightThisMenu:(NSInteger)index
{
    [uiButtonCoupon setImage:[UIImage imageNamed:@"coupon_@2x.png"] forState:UIControlStateNormal];
    [uiButtonFind setImage:[UIImage imageNamed:@"find_@2x.png"] forState:UIControlStateNormal];
    [uiButtonHome setImage:[UIImage imageNamed:@"home_@2x.png"] forState:UIControlStateNormal];
    [uiButtonMore setImage:[UIImage imageNamed:@"menu_@2x.png"] forState:UIControlStateNormal];
    
    switch (index) {
        case 0:
            break;
            
        case 1:
            [uiButtonFind setImage:[UIImage imageNamed:@"find_active_@2x.png"] forState:UIControlStateNormal];
            break;
        default:
            break;
    }
}
@end
