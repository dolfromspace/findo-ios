//
//  MasterDB.h
//  Findo
//
//  Created by Hyunjae on 27/03/15.
//  Copyright (c) 2015 Chromic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class StoreInfo;

@interface MasterDB : NSManagedObject

@property (nonatomic, retain) NSOrderedSet *stores;
@end

@interface MasterDB (CoreDataGeneratedAccessors)

- (void)insertObject:(StoreInfo *)value inStoresAtIndex:(NSUInteger)idx;
- (void)removeObjectFromStoresAtIndex:(NSUInteger)idx;
- (void)insertStores:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeStoresAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInStoresAtIndex:(NSUInteger)idx withObject:(StoreInfo *)value;
- (void)replaceStoresAtIndexes:(NSIndexSet *)indexes withStores:(NSArray *)values;
- (void)addStoresObject:(StoreInfo *)value;
- (void)removeStoresObject:(StoreInfo *)value;
- (void)addStores:(NSOrderedSet *)values;
- (void)removeStores:(NSOrderedSet *)values;
@end
